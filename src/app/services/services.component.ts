import { Component, OnInit } from '@angular/core';
const service = [
  {
    icon: "fa fa-eye",
    text: "lorem ipsum dolor sit"
  },
  {
    icon: "fa fa-ambulance",
    text: "lorem ipsum dolor sit"
  },
  {
    icon: "fa fa-asterisk",
    text: "lorem ipsum dolor sit"
  },
  {
    icon: "fa fa-user",
    text: "lorem ipsum dolor sit"
  },
  {
    icon: "fa fa-life-ring",
    text: "lorem ipsum dolor sit"
  },
  {
    icon: "fa fa-tint",
    text: "lorem ipsum dolor sit"
  },
  {
    icon: "fa fa-shirtsinbulk",
    text: "lorem ipsum dolor sit"
  },
  {
    icon: "fa fa-eye",
    text: "lorem ipsum dolor sit"
  }
]
@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
services = service;
  constructor() { }

  ngOnInit() {
  }

}
