import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SearchComponent } from './search/search.component';
import { AppointmentComponent } from './appointment/appointment.component';
import { MoreComponent } from './more/more.component';
import { ServicesComponent } from './services/services.component';
import { WorkersComponent } from './workers/workers.component';
import { ClinicsComponent } from './clinics/clinics.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchComponent,
    AppointmentComponent,
    MoreComponent,
    ServicesComponent,
    WorkersComponent,
    ClinicsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
