import { Component, OnInit } from '@angular/core';
const mores =[
  {
    icon: "fa fa-hospital-o",
    name: "Hospital",
    info: "lorem ipsum dolor sit"
  },
  {
    icon: "fa fa-briefcase",
    name: "Workers",
    info: "lorem ipsum dolor sit"
  },
  {
    icon: "fa fa-globe",
    name: "Globe",
    info: "lorem ipsum dolor sit"
  }
]
@Component({
  selector: 'app-more',
  templateUrl: './more.component.html',
  styleUrls: ['./more.component.css']
})
export class MoreComponent implements OnInit {
  mores = mores;
  constructor() { }

  ngOnInit() {
  }

}
